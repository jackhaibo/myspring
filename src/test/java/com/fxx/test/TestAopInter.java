package com.fxx.test;

public interface TestAopInter {

    void testAdvice();

    void testNoAdvice();
}
