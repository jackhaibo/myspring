package com.fxx.test;

public class AopTest implements TestAopInter {
    public void testAdvice() {
        System.out.println("使用切面");
    }

    public void testNoAdvice() {
        System.out.println("不使用切面");
    }
}
