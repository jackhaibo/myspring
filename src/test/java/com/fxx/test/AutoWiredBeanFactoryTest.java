package com.fxx.test;

import com.fxx.aop.*;
import com.fxx.bean.BeanDefiniton;
import com.fxx.bean.factory.AutoWiredBeanFactory;
import com.fxx.context.ClasspathApplictionContext;
import com.fxx.io.Resource;
import com.fxx.io.ResourceLoader;
import com.fxx.pojo.Student;
import com.fxx.xml.XmlBeandefinitonReader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Map;

public class AutoWiredBeanFactoryTest {

    private  AutoWiredBeanFactory beanFactory;
    private Student student;
    /*@Before
    public void before() throws Exception {
        ResourceLoader resourceLoader = new ResourceLoader();
        Resource resource = resourceLoader.getResource("application.xml");
        XmlBeandefinitonReader xmlBeandefinitonReader = new XmlBeandefinitonReader(resource);
        xmlBeandefinitonReader.registContent();
        Map<String, BeanDefiniton> rigistry = xmlBeandefinitonReader.getRigistry();
        beanFactory = new AutoWiredBeanFactory();
        for(String key : rigistry.keySet()){
            beanFactory.registBeanDefinition(key,rigistry.get(key));
        }
    }

    @After
    public void after() throws Exception {
        System.out.println(student.getAge());
        System.out.println(student.getName());
        System.out.println(student.getSex().getSex());
    }
    @Test
    public void testLazy() throws Exception {
         student = (Student)beanFactory.getBean("student");
    }



    @Test
    public void testPreLoad() throws Exception {
        beanFactory.singletonBeanInitialize();
        student = (Student)beanFactory.getBean("student");

    }*/

    @Test
    public void testApplcaitonContext() throws Exception {
        ClasspathApplictionContext applictionContext = new ClasspathApplictionContext("application.xml");
        Student student = (Student)applictionContext.getBean("student");

        System.out.println(student.getAge());
        System.out.println(student.getName());
        System.out.println(student.getSex().getSex());
    }


    @Test
    public void testAop() throws Exception {
        AopTest aopTest = new AopTest();
        aopTest.testAdvice();

        TargetBean targetBean = new TargetBean(aopTest);
        //只对接口的方法做切面
        targetBean.addCutPoint(TestAopInter.class.getMethod("testAdvice"));
        MethodInvocationHandller handller = new MethodInvocationHandller(targetBean);
        handller.setAdvice(new Advice() {
            public Object before(Object target, Method method, Object[] args) {
                System.out.println("before");
                return target;
            }

            public Object after(Object result, Object target, Method method, Object[] args) {
                System.out.println("after");
                return result;
            }
        });
        TestAopInter proxy = (TestAopInter)ProxyFactory.getProxy(handller);
        proxy.testAdvice();
        proxy.testNoAdvice();


    }
}
