package com.fxx.io;

import java.net.URL;

public class ResourceLoader {

    public  Resource getResource(String fileName){
        URL resource = this.getClass().getClassLoader().getResource(fileName);
        return new URLResource(resource);
    }
}
