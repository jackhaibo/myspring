package com.fxx.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

public  class MethodInvocationHandller implements InvocationHandler{

    private  final TargetBean targetBean;

    private Advice advice;

    public MethodInvocationHandller(TargetBean targetBean){
        this.targetBean=targetBean;
    }

    public TargetBean getTargetBean() {
        return targetBean;
    }

    public Advice getAdvice() {
        return advice;
    }

    public void setAdvice(Advice advice) {
        this.advice = advice;
    }

    /**
     *
     * @param proxy 代理类本身的一个实例(代理类中)
     * @param method
     * @param args
     * @return
     * @throws Throwable
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String name = method.getName();
        Map<String, List<Method>> methodMap = this.targetBean.getMethodMap();
        Object target = this.targetBean.getTarget();
        if(!methodMap.containsKey(name)){
           return method.invoke(target, args);
        }
        List<Method> methodBeans = methodMap.get(name);
        if(!containMethod(methodBeans,method)){
            return method.invoke(target, args);
        }
        advice.before(target,method,args);
        Object result = method.invoke(target, args);
        Object afterresult = advice.after(result, target, method, args);
        return afterresult;
    }


    private boolean containMethod(List<Method> methodBeans, Method method){
        for(Method mb : methodBeans){
           if(mb.equals(method)){
               return true;
           }
        }
        return false;
    }

}
