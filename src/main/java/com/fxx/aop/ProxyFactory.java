package com.fxx.aop;

import java.lang.reflect.Proxy;

public class ProxyFactory {

    public static Object getProxy(MethodInvocationHandller handler){
        Object target = handler.getTargetBean().getTarget();
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),target.getClass().getInterfaces(),handler);
    }
}
