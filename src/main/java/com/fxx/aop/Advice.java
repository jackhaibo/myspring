package com.fxx.aop;

import java.lang.reflect.Method;

public interface Advice {

    /**
     *
     * @param target 目标对象
     * @param method 目标方法
     * @param args 方法参数
     * @return
     */
     abstract Object before(Object target, Method method, Object[] args);

     abstract Object after(Object result,Object target, Method method, Object[] args);
}
