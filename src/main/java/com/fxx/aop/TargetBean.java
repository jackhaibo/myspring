package com.fxx.aop;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TargetBean {
    private final Object target;
    private Map<String,List<Method>> methodMap = new HashMap<String, List<Method>>();

    public TargetBean(Object target){
        this.target=target;
    }

    public Object getTarget() {
        return target;
    }



    public void addCutPoint(Method methodBean){
        String name = methodBean.getName();
        List<Method> methodBeans = methodMap.get(name);
        if(methodBeans==null){
            methodBeans=new ArrayList<Method>();
            methodMap.put(name,methodBeans);
        }
        methodBeans.add(methodBean);


    }

    public Map<String, List<Method>> getMethodMap() {
        return methodMap;
    }
}
