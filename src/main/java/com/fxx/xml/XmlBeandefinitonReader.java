package com.fxx.xml;

import com.fxx.bean.*;
import com.fxx.io.Resource;
import com.fxx.io.ResourceLoader;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.apache.commons.lang3.StringUtils;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class XmlBeandefinitonReader extends AbstractBeanDefinitionReader{

    //当父类无空参构造器时，必须显示声明相同参数类型构造器，不然子类在初始化之前父类无法初始化
    public XmlBeandefinitonReader(Resource resource){
        super(resource);
    }
    public void registContent() throws Exception {
        InputStream inputStream = this.getResource().getInputStream();
        this.getBeandefinitons(inputStream);
    }

    private void getBeandefinitons(InputStream inputStream) throws Exception {
        SAXBuilder saxBuilder = new SAXBuilder();
        Document doc = saxBuilder.build(inputStream);
        Element rootElement = doc.getRootElement();
        String name = rootElement.getName();
        if(!"beans".equals(name)){
            throw new Exception("xml文件错误，无beans根节点");
        }
        List<Element> children = rootElement.getChildren();
        Map<String, BeanDefiniton> rigistry = this.getRigistry();
        for (Element e: children) {
            String eName = e.getName();
            if(!"bean".equals(eName)){
                break;
            }
            String id = e.getAttributeValue("id");
            String className = e.getAttributeValue("class");
            BeanDefiniton beanDefiniton = new BeanDefiniton();
            beanDefiniton.setBeanClassName(className);
            //装载bean定义
            setProperties(e,beanDefiniton);
            rigistry.put(id,beanDefiniton);
        }

    }

    /**
     * 实现属性读取，当属性为String类型时直接设置，当为其他对象类型时封装为BeanReference后设置
     * @param e
     * @param beanDefiniton
     */
    private void setProperties(Element e,BeanDefiniton beanDefiniton){
        List<Element> children = e.getChildren();
        PropertyValues pvs = beanDefiniton.getPvs();
        if(pvs==null){
            pvs= new PropertyValues();
            beanDefiniton.setPvs(pvs);
        }
        for (Element property: children) {
            String propertyName = property.getName();
            if(!"property".equals(propertyName)){
                break;
            }
            String name = property.getAttributeValue("name");
            PropertyValue propertyValue = null;
            String value = property.getAttributeValue("value");
            String refName = property.getAttributeValue("ref");
            if(StringUtils.isNoneBlank(value)){
                propertyValue = new PropertyValue(name,value);
            }else if(StringUtils.isNoneBlank(refName)){
                BeanReference bf = new BeanReference(refName);
                propertyValue = new PropertyValue(name,bf);
            }else{
                break;
            }
            pvs.addValue(propertyValue);
        }
    }
}
