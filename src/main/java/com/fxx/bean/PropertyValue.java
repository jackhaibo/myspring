package com.fxx.bean;

public class PropertyValue {
    private final String name;
    private final Object value;

    //对象属性属于不可改变的时候，可将其设置为final，在构造器中赋值，并不提供set方法（提供也没用）

    public  PropertyValue(String name,Object value){
        this.name=name;
        this.value=value;
    }
    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }

}
