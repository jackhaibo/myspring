package com.fxx.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于载入属性前判断
 */
public class PropertyValues {
    private final List<PropertyValue> propertyValueList = new ArrayList<PropertyValue>();

    public void addValue(PropertyValue pv){
        //此处判断
        propertyValueList.add(pv);
    }

    public List<PropertyValue> getPropertyValueList() {
        return propertyValueList;
    }
}
