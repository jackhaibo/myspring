package com.fxx.bean.factory;

import com.fxx.bean.BeanDefiniton;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public abstract class AbstractBeanFactory implements BeanFactory{
    private final Map<String,BeanDefiniton> beanDefinitonMap = new ConcurrentHashMap<String, BeanDefiniton>();
    /**
     * 若当前bean已实例化则直接返回bean，若还未创建则创建后返回
     */
    public Object getBean(String name) throws Exception {
        if(!beanDefinitonMap.containsKey(name)||beanDefinitonMap.get(name)==null){
            return null;
        }
        Object bean = beanDefinitonMap.get(name).getBean();
        if(bean==null){
            bean = this.creatBean(beanDefinitonMap.get(name));
        }
        return bean;
    }

    /**
     * beanDefiniton只负责保存bean的定义，不负责bena的实例化
     * 重构将bean创建从beanDefiniton注册中剥离出来，1.方便对象类型的属性注入。2.实现懒加载
     * @param name
     * @param beanDefiniton
     */
    public void registBeanDefinition(String name, BeanDefiniton beanDefiniton){
        if(beanDefiniton==null){
            return;
        }
        beanDefinitonMap.put(name,beanDefiniton);
    }

    /**
     * 所有beanDefiniton注册完后，将实例化注册的所有对象
     */
    public void singletonBeanInitialize() throws Exception{
        Set<String> keys = beanDefinitonMap.keySet();
        for(String key : keys){
            getBean(key);
        }
    }

    protected abstract Object creatBean(BeanDefiniton beanDefiniton) throws Exception;
}
