package com.fxx.bean.factory;

import com.fxx.bean.BeanDefiniton;
import com.fxx.bean.BeanReference;
import com.fxx.bean.PropertyValue;
import com.fxx.bean.PropertyValues;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;

public class AutoWiredBeanFactory extends AbstractBeanFactory{

    /**
     * 实现实例创建以及属性装载
     * @param beanDefiniton
     * @return
     */
    @Override
    protected Object creatBean(BeanDefiniton beanDefiniton)  throws Exception{
        return doCreatBean(beanDefiniton);
    }

   private Object doCreatBean(BeanDefiniton beanDefiniton) throws Exception{
       Object bean = beanDefiniton.getBeanClass().newInstance();
       setPropertyValues(bean,beanDefiniton.getPvs());
       beanDefiniton.setBean(bean);
       return bean;
   }

    /**
     * 在所有beanDefiniton已完成注册的情况下才能保证ref的属性注入！！
     * @param bean
     * @param pvs
     * @throws Exception
     */
   protected void setPropertyValues(Object bean,PropertyValues pvs) throws Exception {
        if(pvs.getPropertyValueList().size()==0){
            return;
        }
        for (PropertyValue pv : pvs.getPropertyValueList()){
            Field declaredField = bean.getClass().getDeclaredField(pv.getName());
            declaredField.setAccessible(true);
            Object value = pv.getValue();
            if(value instanceof BeanReference){
                BeanReference bf = (BeanReference) value;
                value =  this.getBean(bf.getName());
            }
            declaredField.set(bean, value);
        }
   }
}
