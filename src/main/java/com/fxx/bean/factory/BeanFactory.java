package com.fxx.bean.factory;

import com.fxx.bean.BeanDefiniton;

public interface BeanFactory {

    Object getBean(String name) throws Exception;
    void registBeanDefinition(String name, BeanDefiniton beanDefiniton);
}
