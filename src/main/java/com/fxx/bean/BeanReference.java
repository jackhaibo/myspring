package com.fxx.bean;

/**
 * 用于bean定义中为对象的ref封装
 */
public class BeanReference {
    private final String name;
    private Object refBean;
    public BeanReference(String name){
        this.name=name;
    }

    public Object getRefBean() {
        return refBean;
    }

    public void setRefBean(Object refBean) {
        this.refBean = refBean;
    }

    public String getName() {
        return name;
    }
}
