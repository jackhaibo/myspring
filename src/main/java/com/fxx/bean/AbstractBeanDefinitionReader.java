package com.fxx.bean;

import com.fxx.io.Resource;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
/**
 * 抽象类用于规定通用属性，接口用于规定通用方法
 */
public abstract class AbstractBeanDefinitionReader implements BeanDefinitionReader{
    /**
     * 子类要继承的属性
     */
    private Map<String,BeanDefiniton> rigistry ;
    /**
     * 用于获取Resource对象，进而获取输入流
     */
    private Resource resource;

    public AbstractBeanDefinitionReader(Resource resource){
        this.rigistry = new ConcurrentHashMap<String, BeanDefiniton>();
        this.resource = resource;
    }

    public Map<String, BeanDefiniton> getRigistry() {
        return rigistry;
    }

    public Resource getResource() {
        return resource;
    }
}
