package com.fxx.bean;

import java.io.IOException;

public interface BeanDefinitionReader {
    /**
     * 将指定地址的文件定义的bean封装到beanDefinition中
     * BeanDefinitionReader，负责读取xml，封装为对象，BeanFactory负责生产实例
     * @param
     */
    void registContent() throws IOException, Exception;
}
