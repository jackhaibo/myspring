package com.fxx.context;

import com.fxx.bean.BeanDefiniton;
import com.fxx.bean.factory.AutoWiredBeanFactory;
import com.fxx.bean.factory.BeanFactory;
import com.fxx.io.Resource;
import com.fxx.io.ResourceLoader;
import com.fxx.xml.XmlBeandefinitonReader;

import java.util.Map;

public class ClasspathApplictionContext extends AbstractApplicationContext {

    private String fileName;

    public ClasspathApplictionContext(String fileName) throws Exception {
        this(fileName,new AutoWiredBeanFactory());
    }

    public ClasspathApplictionContext(String fileName,BeanFactory beanFactory) throws Exception {
        super(beanFactory);
        this.fileName = fileName;
        refreshContect();
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void refreshContect() throws Exception {
        ResourceLoader resourceLoader = new ResourceLoader();
        Resource resource = resourceLoader.getResource(fileName);
        XmlBeandefinitonReader xmlBeandefinitonReader = new XmlBeandefinitonReader(resource);
        xmlBeandefinitonReader.registContent();
        Map<String, BeanDefiniton> rigistry = xmlBeandefinitonReader.getRigistry();
        beanFactory = new AutoWiredBeanFactory();
        for(String key : rigistry.keySet()){
            beanFactory.registBeanDefinition(key,rigistry.get(key));
        }
    }
}
