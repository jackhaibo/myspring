package com.fxx.context;

import com.fxx.bean.BeanDefiniton;
import com.fxx.bean.factory.BeanFactory;

/**
 * 包装beanfactory，通过文件资源位置一次性注册所有的beanDefinition
 */
public abstract class AbstractApplicationContext implements ApplictionContext {

    protected BeanFactory beanFactory;

    public AbstractApplicationContext(BeanFactory beanFactory){
        this.beanFactory=beanFactory;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    public Object getBean(String name) throws Exception {
        return beanFactory.getBean(name);
    }

    public void registBeanDefinition(String name, BeanDefiniton beanDefiniton) {
        this.beanFactory.registBeanDefinition(name,beanDefiniton);
    }
}
