package com.fxx.context;

import com.fxx.bean.factory.BeanFactory;

public interface ApplictionContext extends BeanFactory {
    void refreshContect() throws Exception;
}
